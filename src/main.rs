#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::collections::HashMap;
use std::fs::File;
use std::io::{Read, Write};
use std::thread;
use std::{
    cmp::{max, min},
    sync::mpsc::{channel, Receiver, Sender},
    time::Instant,
};

use eframe::egui::plot::Polygon;
use eframe::epaint::{Color32, Stroke};
use eframe::{
    egui::{
        self,
        plot::{Legend, Line, PlotPoints},
        widgets,
    },
    epaint::Vec2,
    App,
};
use eframe::{IconData, NativeOptions};
use image::GenericImageView;
use rand::Rng;
use serde::{Deserialize, Serialize};

#[derive(Default, Copy, Clone, Serialize, Deserialize)]
struct Die {
    pub amount: usize,
    pub size: usize,
}

impl Die {
    pub fn roll(&self) -> usize {
        let mut sum = 0;
        for _ in 0..self.amount {
            let num = rand::thread_rng().gen_range(1..self.size + 1);
            sum += num;
        }
        return sum;
    }
}

#[derive(Default, Copy, Clone, Serialize, Deserialize)]
struct Weapon {
    pub base_w: f64,
    pub die: Die,
    pub loaded_dice: usize,
    pub enhancement_bonus: usize,
    pub crit_threat: usize,
    pub crit_multi: usize,
}

#[derive(Default, Copy, Clone, Serialize, Deserialize)]
struct Spell {
    pub die_perlevel: Die,
    pub dmg_perlevel: usize,
    pub max_caster_level: usize,
}

#[derive(Copy, Default, Clone, PartialEq, Serialize, Deserialize)]
pub enum ScalingFactor {
    #[default]
    SpellPower,
    MeleePower,
    RangedPower,
}

fn calculate_probabilities(damage_values: &Vec<usize>) -> HashMap<usize, f64> {
    let total_values = damage_values.len() as f64;
    let mut frequency_map: HashMap<usize, f64> = HashMap::new();

    // Count the occurrences of each damage value
    for damage in damage_values {
        *frequency_map.entry(*damage).or_insert(0.0) += 1.0;
    }

    // Calculate the probability of each damage value
    for (_, frequency) in &mut frequency_map {
        *frequency /= total_values;
    }

    frequency_map
}

#[derive(Copy, Default, Clone, Serialize, Deserialize)]
struct Imbue {
    pub die: Die,
    pub scaling_factor: ScalingFactor,
    pub scaling_amount: usize,
}

#[derive(Default, Clone, Copy, Serialize, Deserialize)]
struct Character {
    pub weapon_type: WeaponType,
    pub mode: Mode,
    pub imbue: Imbue,
    pub has_imbue: bool,
    pub spell_power: usize,
    pub caster_level: usize,
    pub extra_max_caster_levels: usize,
    pub spell_crit_chance: usize,
    pub spell_crit_multi: usize,

    pub maximize: bool,
    pub empower: bool,
    pub intensify: bool,

    pub melee_power: usize,
    pub ranged_power: usize,
    pub doublestrike: usize,
    pub doubleshot: usize,
    pub extra_threat: usize,
    pub extra_multi: usize,
    pub vorpal_multi: usize,

    pub archers_focus_stacks: usize,
    pub extra_focus_stacks: bool,
    pub melee_style: MeleeStyle,
    pub ranged_style: RangedStyle,

    pub damage_mod: usize,
    pub extra_w: f64,
    pub crit_damage_mod: usize,
    pub sneak_damage: usize,
    pub sneak_dice: Die,

    pub offhand_strikechance: usize,
    pub offhand_doublestrike: usize,

    pub auto_offhand: bool,
    pub subtract_enhancment: bool,
    pub subtract_base_from_crit: bool,
}

#[derive(Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum Mode {
    #[default]
    Weapon,
    Spell,
}

#[derive(Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum WeaponType {
    #[default]
    Melee,
    Ranged,
}

#[derive(Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum MeleeStyle {
    #[default]
    OneWeapon, //THF+SWF
    TwoWeapons, //Two Weapon Fighting
    Handwraps,  //Handwraps (Same as two weapon but same weapon in each)
}

#[derive(Default, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum RangedStyle {
    #[default]
    Other, //200% cap
    Throwing,    //300%
    Shuriken,    //400%
    Inquisitive, //50% 200% cap
    Repeater,    //33% 200% cap
}

fn roll_weapon(char: Character, wep: Weapon) -> usize {
    let attack_roll = Die {
        amount: 1,
        size: 20,
    }
    .roll();
    if attack_roll <= 1 {
        return 0;
    }
    let enhance = match char.subtract_enhancment {
        true => 0,
        false => wep.enhancement_bonus,
    };
    let mut base_damage = ((wep.base_w + char.extra_w)
        * (wep.die.roll() as f64 + wep.loaded_dice as f64)
        + enhance as f64) as usize;
    if attack_roll >= wep.crit_threat - char.extra_threat {
        // this is a crit
        match char.subtract_base_from_crit {
            true => base_damage += char.crit_damage_mod,
            false => base_damage += char.damage_mod + char.crit_damage_mod,
        }

        if attack_roll >= 19 {
            // this is a vorpal
            base_damage *= wep.crit_multi + char.extra_multi + char.vorpal_multi;
        } else {
            base_damage *= wep.crit_multi + char.extra_multi;
        }
    }

    let mut sneak_damage = char.sneak_dice.roll() + char.sneak_damage;
    // add melee/ranged power
    let scaling_power = match char.weapon_type {
        WeaponType::Melee => char.melee_power,
        WeaponType::Ranged => char.ranged_power + (5 * char.archers_focus_stacks),
    };
    base_damage = (base_damage as f64 * (1.0 + (scaling_power as f64 / 100.0))) as usize;
    sneak_damage = (sneak_damage as f64 * (1.0 + ((scaling_power as f64 * 2.0) / 100.0))) as usize;

    let mut imbue_damage = 0;

    if char.has_imbue {
        imbue_damage = char.imbue.die.roll();
        let imbue_scaling_factor = match char.imbue.scaling_factor {
            ScalingFactor::SpellPower => char.spell_power,
            ScalingFactor::MeleePower => char.melee_power,
            ScalingFactor::RangedPower => char.ranged_power,
        };
        imbue_damage = (imbue_damage as f64
            * (1.0
                + ((imbue_scaling_factor as f64 * (char.imbue.scaling_amount as f64 / 100.0))
                    / 100.0))) as usize;
    }

    return base_damage + imbue_damage + sneak_damage;
}

fn calculate_weapon_damage(
    char: Character,
    w1: Weapon,
    w2: Weapon,
    rolls: usize,
    dtx: Sender<usize>,
) {
    for _ in 0..rolls {
        let mut damage = 0;
        match char.weapon_type {
            WeaponType::Melee => {
                let ds_roll = rand::thread_rng().gen_range(1..=100);
                let offhand_roll = rand::thread_rng().gen_range(1..=100);
                let dmg = roll_weapon(char, w1);
                damage += dmg;
                if ds_roll <= char.doublestrike {
                    damage += dmg;
                }
                match char.melee_style {
                    MeleeStyle::OneWeapon => {}
                    MeleeStyle::TwoWeapons => {
                        if offhand_roll <= char.offhand_strikechance {
                            let dmg2 = roll_weapon(char, w2);
                            damage += dmg2;
                            let ds_roll2 = rand::thread_rng().gen_range(1..=100);
                            if ds_roll2 <= char.offhand_doublestrike {
                                damage += dmg2;
                            }
                        }
                    }
                    MeleeStyle::Handwraps => {
                        if offhand_roll <= char.offhand_strikechance {
                            let dmg2 = roll_weapon(char, w1);
                            damage += dmg2;
                            let ds_roll2 = rand::thread_rng().gen_range(1..=100);
                            if ds_roll2 <= char.offhand_doublestrike {
                                damage += dmg2;
                            }
                        }
                    }
                }
            }
            WeaponType::Ranged => {
                let mut double_shot = match char.ranged_style {
                    RangedStyle::Other => min(char.doubleshot, 200),
                    RangedStyle::Throwing => min(char.doubleshot, 300),
                    RangedStyle::Shuriken => min(char.doubleshot, 400),
                    RangedStyle::Inquisitive => min(char.doubleshot / 2, 200),
                    RangedStyle::Repeater => min(char.doubleshot / 3, 200),
                };
                let mut double_multi = 1;
                while double_shot > 100 {
                    double_multi += 1;
                    double_shot -= 100;
                }
                let rolls = match char.ranged_style {
                    RangedStyle::Inquisitive => 2,
                    RangedStyle::Repeater => 3,
                    _ => 1,
                };
                for _ in 0..rolls {
                    let ds_roll = rand::thread_rng().gen_range(1..=100);
                    if ds_roll <= double_shot {
                        damage += roll_weapon(char, w1) * (double_multi + 1);
                    } else {
                        damage += roll_weapon(char, w1) * double_multi;
                    }
                }
            }
        }

        if let Err(_) = dtx.send(damage) {
            return;
        }
    }
}

struct MyApp {
    title: String,
    polys: bool,

    dtx: Sender<usize>,
    drx: Receiver<usize>,

    cdtx: Sender<CharData>,
    cdrx: Receiver<CharData>,

    thread_count: usize,

    damage_values: Vec<usize>,
    stored_series: Vec<(String, Color32, Vec<usize>)>,
    average_damage: f64,
    remaining_rolls: usize,
    total_rolls: usize,
    total_damage: usize,
    rolled_rolls: usize,

    character: Character,
    weapon: Weapon,
    offhand_weapon: Weapon,
    spell: Spell,

    low_damage: usize,
    high_damage: usize,

    exclude_ones: bool,

    datas: Vec<CharData>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct CharData {
    title: String,
    character: Character,
    weapon: Weapon,
    offhand_weapon: Weapon,
    spell: Spell,
    average_damage: f64,
    total_damage: usize,
    rolled_rolls: usize,
}

impl Default for MyApp {
    fn default() -> Self {
        let (dtx, drx) = channel();
        let (cdtx, cdrx) = channel();

        let mut char = Character::default();
        char.imbue.scaling_amount = 100;
        char.sneak_dice.size = 6;

        Self {
            title: String::new(),
            datas: Vec::new(),
            polys: false,
            dtx,
            drx,
            cdtx,
            cdrx,
            thread_count: 16,

            damage_values: Vec::with_capacity(10_000),
            stored_series: Vec::with_capacity(100),
            average_damage: 0.0,
            remaining_rolls: 0,
            total_rolls: 10_000,
            total_damage: 0,
            rolled_rolls: 0,
            low_damage: 100000000,
            high_damage: 0,
            exclude_ones: false,

            character: char,
            weapon: Weapon::default(),
            offhand_weapon: Weapon::default(),
            spell: Spell::default(),
        }
    }
}

impl App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        if self.datas.len() > 0 {
            egui::SidePanel::left("Character Bar").show(ctx, |ui| {
                ui.heading("Available Characters");
                ui.separator();
                let mut to_remove: Option<String> = None;
                for data in self.datas.iter() {
                    ui.horizontal(|ui| {
                        if ui
                            .selectable_value(&mut self.title, data.title.clone(), &data.title)
                            .clicked()
                        {
                            self.character = data.character;
                            self.weapon = data.weapon;
                            self.offhand_weapon = data.offhand_weapon;
                            self.spell = data.spell;
                            self.title = data.title.clone();
                            self.average_damage = data.average_damage;
                            self.rolled_rolls = data.rolled_rolls;
                            self.total_damage = data.total_damage;
                            let (dtx, drx) = channel();
                            self.dtx = dtx;
                            self.drx = drx;
                        }
                        if ui
                            .button(egui::RichText::new("X").color(egui::Color32::RED))
                            .clicked()
                        {
                            to_remove = Some(data.title.clone());
                        }
                    });
                }
                if let Some(target) = to_remove {
                    self.datas = self
                        .datas
                        .clone()
                        .into_iter()
                        .filter(|e| e.title != target)
                        .collect();
                }
            });
        }
        egui::TopBottomPanel::top("Menu Bar").show(ctx, |ui| {
            ui.columns(4, |cols| {
                cols[0].selectable_value(&mut self.character.mode, Mode::Weapon, "Weapon");
                cols[1].selectable_value(&mut self.character.mode, Mode::Spell, "Spell");
                cols[2].horizontal(|ui| {
                    ui.label("|");
                    if ui.button("New").clicked() {
                        self.title = String::new();
                        let mut char = Character::default();
                        char.imbue.scaling_amount = 100;
                        char.sneak_dice.size = 6;
                        self.character = char;
                        self.weapon = Weapon::default();
                        self.offhand_weapon = Weapon::default();
                        self.spell = Spell::default();
                    }
                    if ui.button("Save").clicked() {
                        let data: CharData = CharData {
                            title: self.title.clone(),
                            character: self.character,
                            weapon: self.weapon,
                            offhand_weapon: self.offhand_weapon,
                            spell: self.spell,
                            average_damage: self.average_damage,
                            total_damage: self.total_damage,
                            rolled_rolls: self.rolled_rolls,
                        };
                        thread::spawn(move || {
                            if let Some(path) = rfd::FileDialog::new()
                                .set_title("Save Character Info")
                                .add_filter("damage info", &["ddodi"])
                                .save_file()
                            {
                                if let Ok(json) = serde_json::to_string::<CharData>(&data) {
                                    if let Ok(mut file) = File::create(path) {
                                        let _ = file.write_all(json.as_bytes());
                                    }
                                }
                            }
                        });
                    }
                    if ui.button("Store").clicked() {
                        if !self.title.trim().is_empty() {
                            let data: CharData = CharData {
                                title: self.title.clone(),
                                character: self.character,
                                weapon: self.weapon,
                                offhand_weapon: self.offhand_weapon,
                                spell: self.spell,
                                average_damage: self.average_damage,
                                total_damage: self.total_damage,
                                rolled_rolls: self.rolled_rolls,
                            };
                            self.datas.push(data);
                        }
                    }
                    if ui.button("Load File").clicked() {
                        let cdtx = self.cdtx.clone();
                        thread::spawn(move || {
                            if let Some(paths) = rfd::FileDialog::new()
                                .set_title("Load Character Info")
                                .add_filter("damage info", &["ddodi"])
                                .pick_files()
                            {
                                for path in paths {
                                    if let Ok(mut file) = File::open(path) {
                                        let mut json_data = String::new();
                                        if let Err(_) = file.read_to_string(&mut json_data) {
                                            return;
                                        }
                                        let char_data: Result<CharData, _> =
                                            serde_json::from_str(&json_data);
                                        if let Ok(data) = char_data {
                                            let _ = cdtx.send(data);
                                        }
                                    }
                                }
                            }
                        });
                    }
                    ui.label("|");
                    while let Ok(data) = self.cdrx.try_recv() {
                        self.datas.push(data);
                    }
                });
                cols[3].horizontal(|ui| {
                    egui::widgets::global_dark_light_mode_buttons(ui);
                    ui.checkbox(&mut self.polys, "Fill Graph?");
                });
            });
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.horizontal(|ui| {
                    ui.heading("Character");
                    ui.add(widgets::TextEdit::singleline(&mut self.title));
                });
                match self.character.mode {
                    Mode::Weapon => {
                        if self.character.has_imbue
                            && self.character.imbue.scaling_factor == ScalingFactor::SpellPower
                        {
                            ui.horizontal(|ui| {
                                ui.label("Spellpower:");
                                ui.add(widgets::DragValue::new(&mut self.character.spell_power));
                            });
                        }

                        ui.horizontal(|ui| {
                            ui.label("Damage Mod:");
                            ui.add(widgets::DragValue::new(&mut self.character.damage_mod));
                            ui.label("Crit Damage Mod:");
                            ui.add(widgets::DragValue::new(&mut self.character.crit_damage_mod));
                            ui.label("Crit Excludes Base Mod.:");
                            ui.checkbox(&mut self.character.subtract_base_from_crit, "");
                            ui.label("Sub Weap. Bonus:");
                            ui.checkbox(&mut self.character.subtract_enhancment, "");
                        });

                        ui.horizontal(|ui| {
                            ui.label("Extra +W:");
                            ui.add(
                                widgets::DragValue::new(&mut self.character.extra_w)
                                    .speed(0.25)
                                    .clamp_range(0.0..=100.0)
                                    .max_decimals(2),
                            );
                            ui.label("Extra Crit Threat:");
                            ui.add(
                                widgets::DragValue::new(&mut self.character.extra_threat)
                                    .clamp_range(0..=self.weapon.crit_threat)
                                    .custom_formatter(|x, _| format!("+{}", x)),
                            );
                            ui.label("Extra Crit Multi:");
                            ui.add(
                                widgets::DragValue::new(&mut self.character.extra_multi)
                                    .custom_formatter(|x, _| format!("+{}x", x)),
                            );
                            ui.label("Extra Crit 19-20 Multi:");
                            ui.add(
                                widgets::DragValue::new(&mut self.character.vorpal_multi)
                                    .custom_formatter(|x, _| format!("+{}x", x)),
                            );
                        });

                        ui.horizontal(|ui| {
                            ui.label("Sneak Dice:");
                            ui.add(widgets::DragValue::new(
                                &mut self.character.sneak_dice.amount,
                            ));
                            ui.label("d6");
                            ui.label("Sneak Damage:");
                            ui.add(widgets::DragValue::new(&mut self.character.sneak_damage));
                        });

                        match self.character.weapon_type {
                            WeaponType::Melee => {
                                ui.horizontal(|ui| {
                                    ui.label("Melee Power:");
                                    ui.add(widgets::DragValue::new(
                                        &mut self.character.melee_power,
                                    ));
                                    ui.label("Doublestrike:");
                                    ui.add(
                                        widgets::DragValue::new(&mut self.character.doublestrike)
                                            .clamp_range(0..=100)
                                            .custom_formatter(|x, _| format!("{}%", x)),
                                    );
                                });
                                ui.horizontal(|ui| {
                                    if self.character.melee_style != MeleeStyle::OneWeapon {
                                        ui.label("Offhand Strikechance:");
                                        ui.add(
                                            widgets::DragValue::new(
                                                &mut self.character.offhand_strikechance,
                                            )
                                            .clamp_range(0..=100)
                                            .custom_formatter(|x, _| format!("{}%", x)),
                                        );
                                        if self.character.auto_offhand {
                                            self.character.offhand_doublestrike =
                                                self.character.doublestrike / 2;
                                            ui.label(format!(
                                                "Offhand Doublestrike: {}%",
                                                self.character.offhand_doublestrike
                                            ));
                                        } else {
                                            ui.label("Offhand Doublestrike:");
                                            ui.add(
                                                widgets::DragValue::new(
                                                    &mut self.character.offhand_doublestrike,
                                                )
                                                .clamp_range(0..=100)
                                                .custom_formatter(|x, _| format!("{}%", x)),
                                            );
                                        }

                                        ui.label("Auto offhand:");
                                        ui.checkbox(&mut self.character.auto_offhand, "");
                                    }
                                });
                            }
                            WeaponType::Ranged => {
                                ui.horizontal(|ui| {
                                    ui.label("Ranged Power:");
                                    ui.add(widgets::DragValue::new(
                                        &mut self.character.ranged_power,
                                    ));
                                    ui.label("Doubleshot:");
                                    ui.add(
                                        widgets::DragValue::new(&mut self.character.doubleshot)
                                            .clamp_range(match self.character.ranged_style {
                                                RangedStyle::Other => 0..=200,
                                                RangedStyle::Throwing => 0..=300,
                                                RangedStyle::Shuriken => 0..=400,
                                                RangedStyle::Inquisitive => 0..=200,
                                                RangedStyle::Repeater => 0..=200,
                                            })
                                            .custom_formatter(|x, _| format!("{}%", x)),
                                    );
                                });
                                ui.horizontal(|ui| {
                                    ui.label("Arhcers Focus:");
                                    ui.add(widgets::Slider::new(
                                        &mut self.character.archers_focus_stacks,
                                        0..=match self.character.extra_focus_stacks {
                                            true => 25,
                                            false => 15,
                                        },
                                    ));
                                    ui.label("Extra Stacks:");
                                    ui.checkbox(&mut self.character.extra_focus_stacks, "");
                                });
                            }
                        }
                    }
                    Mode::Spell => {
                        ui.horizontal(|ui| {
                            ui.checkbox(&mut self.character.maximize, "Maximize");
                            ui.checkbox(&mut self.character.empower, "Empower");
                            ui.checkbox(&mut self.character.intensify, "Intensify");
                        });
                        ui.horizontal(|ui| {
                            ui.label("Spellpower:");
                            ui.add(widgets::DragValue::new(&mut self.character.spell_power));
                            ui.label("Spell Crit Chance:");
                            ui.add(
                                widgets::DragValue::new(&mut self.character.spell_crit_chance)
                                    .clamp_range(0..=100)
                                    .custom_formatter(|x, _| format!("{}%", x)),
                            );
                            ui.label("Spell Crit Damage:");
                            ui.add(
                                widgets::DragValue::new(&mut self.character.spell_crit_multi)
                                    .custom_formatter(|x, _| format!("{}%", x)),
                            );
                        });
                    }
                }
                ui.separator();
                match self.character.mode {
                    Mode::Weapon => {
                        ui.horizontal(|ui| {
                            ui.heading("Imbue");
                            ui.checkbox(&mut self.character.has_imbue, "");
                        });
                        if self.character.has_imbue {
                            ui.horizontal(|ui| {
                                ui.label("Imbue Dice:");
                                ui.add(widgets::DragValue::new(
                                    &mut self.character.imbue.die.amount,
                                ));
                                ui.label("d");
                                ui.add(widgets::DragValue::new(&mut self.character.imbue.die.size));
                            });
                            ui.horizontal(|ui| {
                                ui.label("Scaling Factor:");
                                ui.selectable_value(
                                    &mut self.character.imbue.scaling_factor,
                                    ScalingFactor::SpellPower,
                                    "Spell Power",
                                );

                                ui.selectable_value(
                                    &mut self.character.imbue.scaling_factor,
                                    ScalingFactor::MeleePower,
                                    "Melee Power",
                                );
                                ui.selectable_value(
                                    &mut self.character.imbue.scaling_factor,
                                    ScalingFactor::RangedPower,
                                    "Ranged Power",
                                );
                                ui.add(
                                    widgets::DragValue::new(
                                        &mut self.character.imbue.scaling_amount,
                                    )
                                    .custom_formatter(|x, _| format!("{}%", x)),
                                );
                            });
                        }
                        ui.separator();
                        ui.heading("Weapon");
                        ui.horizontal(|ui| {
                            ui.selectable_value(
                                &mut self.character.weapon_type,
                                WeaponType::Melee,
                                "Melee",
                            );
                            ui.selectable_value(
                                &mut self.character.weapon_type,
                                WeaponType::Ranged,
                                "Ranged",
                            );
                        });
                        ui.horizontal(|ui| match self.character.weapon_type {
                            WeaponType::Melee => {
                                ui.selectable_value(
                                    &mut self.character.melee_style,
                                    MeleeStyle::OneWeapon,
                                    "One Weapon",
                                );
                                ui.selectable_value(
                                    &mut self.character.melee_style,
                                    MeleeStyle::TwoWeapons,
                                    "Two Weapons",
                                );
                                ui.selectable_value(
                                    &mut self.character.melee_style,
                                    MeleeStyle::Handwraps,
                                    "Handwraps",
                                );
                                if self.character.melee_style == MeleeStyle::Handwraps {
                                    self.offhand_weapon = self.weapon.clone();
                                }
                            }
                            WeaponType::Ranged => {
                                ui.selectable_value(
                                    &mut self.character.ranged_style,
                                    RangedStyle::Other,
                                    "Other",
                                );
                                ui.selectable_value(
                                    &mut self.character.ranged_style,
                                    RangedStyle::Throwing,
                                    "Throwing",
                                );
                                ui.selectable_value(
                                    &mut self.character.ranged_style,
                                    RangedStyle::Shuriken,
                                    "Shuriken",
                                );
                                ui.selectable_value(
                                    &mut self.character.ranged_style,
                                    RangedStyle::Inquisitive,
                                    "Inquisitive",
                                );
                                ui.selectable_value(
                                    &mut self.character.ranged_style,
                                    RangedStyle::Repeater,
                                    "Repeater",
                                );
                            }
                        });
                        ui.horizontal(|ui| {
                            ui.label("Weapon:");
                            ui.add(
                                widgets::DragValue::new(&mut self.weapon.base_w)
                                    .max_decimals(2)
                                    .speed(0.25)
                                    .clamp_range(0.0..=100.0),
                            );
                            ui.label("(");
                            ui.add(widgets::DragValue::new(&mut self.weapon.die.amount));
                            ui.label("d");
                            ui.add(widgets::DragValue::new(&mut self.weapon.die.size));
                            ui.label("+");
                            ui.add(widgets::DragValue::new(&mut self.weapon.loaded_dice));
                            ui.label(") +");
                            ui.add(widgets::DragValue::new(&mut self.weapon.enhancement_bonus));
                            ui.label("   ");
                            ui.add(
                                widgets::DragValue::new(&mut self.weapon.crit_threat)
                                    .clamp_range(0..=20),
                            );
                            ui.label("- 20   x");
                            ui.add(widgets::DragValue::new(&mut self.weapon.crit_multi));
                        });

                        if self.character.weapon_type == WeaponType::Melee
                            && self.character.melee_style == MeleeStyle::TwoWeapons
                        {
                            ui.horizontal(|ui| {
                                ui.label("Offhand:");
                                ui.add(
                                    widgets::DragValue::new(&mut self.offhand_weapon.base_w)
                                        .max_decimals(2)
                                        .speed(0.25)
                                        .clamp_range(0.0..=100.0),
                                );
                                ui.label("(");
                                ui.add(widgets::DragValue::new(
                                    &mut self.offhand_weapon.die.amount,
                                ));
                                ui.label("d");
                                ui.add(widgets::DragValue::new(&mut self.offhand_weapon.die.size));
                                ui.label("+");
                                ui.add(widgets::DragValue::new(
                                    &mut self.offhand_weapon.loaded_dice,
                                ));
                                ui.label(") +");
                                ui.add(widgets::DragValue::new(
                                    &mut self.offhand_weapon.enhancement_bonus,
                                ));
                                ui.label("   ");
                                ui.add(
                                    widgets::DragValue::new(&mut self.offhand_weapon.crit_threat)
                                        .clamp_range(0..=20),
                                );
                                ui.label("- 20   x");
                                ui.add(widgets::DragValue::new(
                                    &mut self.offhand_weapon.crit_multi,
                                ));
                            });
                        }
                        // DONE WITH WEAPON MENU
                    }
                    Mode::Spell => {
                        ui.heading("Spell");
                    }
                }
                ui.separator();

                ui.horizontal(|ui| {
                    let old_total = self.total_rolls;
                    ui.label("Total Rolls:");
                    ui.add(widgets::DragValue::new(&mut self.total_rolls));
                    ui.label("Threads:");
                    ui.add(widgets::DragValue::new(&mut self.thread_count).clamp_range(1..=128));
                    if self.total_rolls > old_total {
                        self.damage_values.reserve(self.total_rolls - old_total);
                    }
                    if self.remaining_rolls == 0 {
                        if ui.button("Calculate Damage!").clicked() {
                            self.remaining_rolls = self.total_rolls;
                            let mut partitioned_rolls = self.total_rolls;
                            let rolls_per = self.total_rolls / self.thread_count;
                            for _ in 0..self.thread_count - 1 {
                                let allocated_rolls =
                                    min(rolls_per, max(partitioned_rolls - rolls_per, 0));
                                partitioned_rolls -= allocated_rolls;
                                let dtx = self.dtx.clone();
                                let char = self.character.clone();
                                let w1 = self.weapon.clone();
                                let w2 = self.offhand_weapon.clone();
                                std::thread::spawn(move || {
                                    calculate_weapon_damage(char, w1, w2, allocated_rolls, dtx);
                                });
                            }
                            if partitioned_rolls > 0 {
                                let dtx = self.dtx.clone();
                                let char = self.character.clone();
                                let w1 = self.weapon.clone();
                                let w2 = self.offhand_weapon.clone();
                                std::thread::spawn(move || {
                                    calculate_weapon_damage(char, w1, w2, partitioned_rolls, dtx);
                                });
                            }
                        }
                    }
                    if self.damage_values.len() > 0 {
                        if ui.button("Clear Current Series").clicked() {
                            self.damage_values.clear();
                            self.remaining_rolls = 0;
                            self.rolled_rolls = 0;
                            self.low_damage = 10000000;
                            self.high_damage = 0;
                            self.average_damage = 0.0;
                            self.total_damage = 0;
                            let (dtx, drx) = channel::<usize>();
                            self.drx = drx;
                            self.dtx = dtx;
                        }
                    }

                    ui.label(format!("Remaining Rolls: {}", self.remaining_rolls));
                    ui.label("Exclude rolls on 1s: ");
                    ui.checkbox(&mut self.exclude_ones, "");
                });
                ui.horizontal(|ui| {
                    if self.damage_values.len() > 0 {
                        if ui.button("Clear ALL Calculations").clicked() {
                            self.stored_series.clear();
                            self.damage_values.clear();
                            self.remaining_rolls = 0;
                            self.rolled_rolls = 0;
                            self.low_damage = 10000000;
                            self.high_damage = 0;
                            self.average_damage = 0.0;
                            self.total_damage = 0;
                            let (dtx, drx) = channel::<usize>();
                            self.drx = drx;
                            self.dtx = dtx;
                        }
                    }
                    if self.stored_series.len() > 0 {
                        if ui.button("Clear Stored").clicked() {
                            self.stored_series.clear();
                        }
                    }
                    if self.damage_values.len() > 0 {
                        if ui.button("Stow Calculations").clicked() {
                            let color_rotation = vec![
                                Color32::BLUE,
                                Color32::GREEN,
                                Color32::YELLOW,
                                Color32::LIGHT_RED,
                                Color32::LIGHT_BLUE,
                                Color32::LIGHT_GREEN,
                                Color32::LIGHT_YELLOW,
                                Color32::DARK_RED,
                                Color32::DARK_BLUE,
                                Color32::DARK_GREEN,
                                Color32::GOLD,
                            ];
                            self.stored_series.push((
                                format!("{} avg:{:.2}", self.title.clone(), self.average_damage),
                                color_rotation[self.stored_series.len() % color_rotation.len()],
                                self.damage_values.clone(),
                            ));
                            self.damage_values.clear();
                            self.remaining_rolls = 0;
                            self.rolled_rolls = 0;
                            self.average_damage = 0.0;
                            self.total_damage = 0;
                            let (dtx, drx) = channel::<usize>();
                            self.drx = drx;
                            self.dtx = dtx;
                        }
                    }
                });
                ui.heading(format!("Average Damage: {:.1}", self.average_damage));
                ui.heading(format!(
                    "Total Damage: {} over {} Rolls",
                    self.total_damage, self.rolled_rolls
                ));

                if self.remaining_rolls > 0 {
                    let tick = Instant::now();
                    while let Ok(dmg_roll) = self.drx.try_recv() {
                        if self.remaining_rolls > 0 {
                            self.remaining_rolls -= 1;
                        }

                        self.rolled_rolls += 1;
                        if self.exclude_ones && dmg_roll == 0 {
                            continue;
                        }
                        if dmg_roll > self.high_damage {
                            self.high_damage = dmg_roll;
                        }
                        if dmg_roll < self.low_damage {
                            self.low_damage = dmg_roll;
                        }
                        self.damage_values.push(dmg_roll);

                        //TODO: do rolling here!
                        if tick.elapsed().as_micros() > 100 {
                            break;
                        }
                    }
                    self.average_damage = self.damage_values.iter().sum::<usize>() as f64
                        / self.damage_values.len() as f64;
                    self.total_damage = self.damage_values.iter().sum::<usize>();
                    ctx.request_repaint();
                }

                //TODO: Display damage graphs here

                ui.separator();
                ui.heading("Damage Graphs");
                ui.collapsing("Run Graph", |ui| {
                    egui::plot::Plot::new("run graph")
                        .auto_bounds_x()
                        .auto_bounds_y()
                        .min_size(Vec2::new(800.0, 350.0))
                        .legend(Legend::default().position(egui::plot::Corner::LeftTop))
                        .show(ui, |plot_ui| {
                            for (title, color, values) in &self.stored_series {
                                let plot_points: PlotPoints = values
                                    .iter()
                                    .enumerate()
                                    .map(|(i, y)| [i as f64, *y as f64])
                                    .collect();
                                plot_ui.line(
                                    Line::new(plot_points)
                                        .color(*color)
                                        .name(format!("Stored Run: {}", title)),
                                );
                            }
                            let plot_points: PlotPoints = self
                                .damage_values
                                .iter()
                                .enumerate()
                                .map(|(i, y)| [i as f64, *y as f64])
                                .collect();
                            plot_ui.line(
                                Line::new(plot_points)
                                    .color(egui::Color32::RED)
                                    .name("Current Run"),
                            );

                            plot_ui.line(
                                Line::new(vec![
                                    [0.0, self.low_damage as f64],
                                    [self.damage_values.len() as f64, self.low_damage as f64],
                                ])
                                .name("Min. Damage"),
                            );
                            plot_ui.line(
                                Line::new(vec![
                                    [0.0, self.average_damage],
                                    [self.damage_values.len() as f64, self.average_damage],
                                ])
                                .name("Avg. Damage"),
                            );
                            plot_ui.line(
                                Line::new(vec![
                                    [0.0, self.high_damage as f64],
                                    [self.damage_values.len() as f64, self.high_damage as f64],
                                ])
                                .name("Max. Damage"),
                            );
                        });
                });
                ui.collapsing("Damage Distribution", |ui| {
                    egui::plot::Plot::new("damage distrubtions")
                        .auto_bounds_x()
                        .auto_bounds_y()
                        .min_size(Vec2::new(800.0, 350.0))
                        .legend(Legend::default().position(egui::plot::Corner::LeftTop))
                        .show(ui, |plot_ui| {
                            let probabilities = calculate_probabilities(&self.damage_values);
                            let mut start = match self.exclude_ones {
                                true => self.low_damage,
                                false => 0,
                            };
                            if start > self.high_damage {
                                start = self.high_damage;
                            }
                            let mut histogram_data: Vec<[f64; 2]> =
                                Vec::with_capacity(max(100, self.high_damage - start));
                            let mut raw_values: Vec<(usize, f64)> = Vec::new();
                            for (k, v) in &probabilities {
                                raw_values.push((*k, *v));
                            }
                            raw_values.sort_by(|a, b| a.0.cmp(&b.0));
                            let max_damage = raw_values.last().unwrap_or(&(0, 0.0)).1;
                            for i1 in 0..raw_values.len() - 1 {
                                if raw_values.len() < 2 {
                                    break;
                                }
                                let (x1, y1) = raw_values[i1];
                                let (x2, y2) = raw_values[i1 + 1];

                                if (x2 - x1) as f64 > 0.1 * max_damage as f64 && (x2 - x1) > 3 {
                                    let x11 = x1 + 1;
                                    let y11 = 0.0;
                                    let x21 = x2 - 1;
                                    let y21 = 0.0;
                                    histogram_data.push([x1 as f64, y1]);
                                    histogram_data.push([x11 as f64, y11]);
                                    if self.polys {
                                        plot_ui.polygon(
                                            Polygon::new(vec![
                                                [x1 as f64, 0.0],
                                                [x1 as f64, y1],
                                                [x11 as f64, y11],
                                                [x11 as f64, 0.0],
                                            ])
                                            .stroke(Stroke::NONE)
                                            .color(egui::Color32::RED)
                                            .name("Current Distribution"),
                                        );
                                    }
                                    histogram_data.push([x21 as f64, y21]);
                                    histogram_data.push([x2 as f64, y2]);
                                    if self.polys {
                                        plot_ui.polygon(
                                            Polygon::new(vec![
                                                [x21 as f64, 0.0],
                                                [x21 as f64, y21],
                                                [x2 as f64, y2],
                                                [x2 as f64, 0.0],
                                            ])
                                            .stroke(Stroke::NONE)
                                            .color(egui::Color32::RED)
                                            .name("Current Distribution"),
                                        );
                                    }
                                } else {
                                    histogram_data.push([x1 as f64, y1]);
                                    histogram_data.push([x2 as f64, y2]);

                                    if self.polys {
                                        plot_ui.polygon(
                                            Polygon::new(vec![
                                                [x1 as f64, 0.0],
                                                [x1 as f64, y1],
                                                [x2 as f64, y2],
                                                [x2 as f64, 0.0],
                                            ])
                                            .stroke(Stroke::NONE)
                                            .color(egui::Color32::RED)
                                            .name("Current Distribution"),
                                        );
                                    }
                                }
                            }

                            plot_ui.line(
                                Line::new(histogram_data)
                                    .name("Current Distribution")
                                    .color(egui::Color32::RED),
                            );

                            for (title, color, values) in &self.stored_series {
                                let probabilities = calculate_probabilities(&values);
                                let start = 0;
                                let mut histogram_data: Vec<[f64; 2]> =
                                    Vec::with_capacity(max(100, self.high_damage - start));
                                let mut raw_values: Vec<(usize, f64)> = Vec::new();
                                for (k, v) in &probabilities {
                                    raw_values.push((*k, *v));
                                }
                                raw_values.sort_by(|a, b| a.0.cmp(&b.0));
                                let max_damage = raw_values.last().unwrap_or(&(0, 0.0)).1;
                                for i1 in 0..raw_values.len() - 1 {
                                    if raw_values.len() < 2 {
                                        break;
                                    }
                                    let (x1, y1) = raw_values[i1];
                                    let (x2, y2) = raw_values[i1 + 1];

                                    if (x2 - x1) as f64 > 0.1 * max_damage as f64 && (x2 - x1) > 3 {
                                        let x11 = x1 + 1;
                                        let y11 = 0.0;
                                        let x21 = x2 - 1;
                                        let y21 = 0.0;
                                        histogram_data.push([x1 as f64, y1]);
                                        histogram_data.push([x11 as f64, y11]);
                                        if self.polys {
                                            plot_ui.polygon(
                                                Polygon::new(vec![
                                                    [x1 as f64, 0.0],
                                                    [x1 as f64, y1],
                                                    [x11 as f64, y11],
                                                    [x11 as f64, 0.0],
                                                ])
                                                .stroke(Stroke::NONE)
                                                .color(*color)
                                                .name(format!("Stored: {}", title)),
                                            );
                                        }
                                        histogram_data.push([x21 as f64, y21]);
                                        histogram_data.push([x2 as f64, y2]);
                                        if self.polys {
                                            plot_ui.polygon(
                                                Polygon::new(vec![
                                                    [x21 as f64, 0.0],
                                                    [x21 as f64, y21],
                                                    [x2 as f64, y2],
                                                    [x2 as f64, 0.0],
                                                ])
                                                .stroke(Stroke::NONE)
                                                .color(*color)
                                                .name(format!("Stored: {}", title)),
                                            );
                                        }
                                    } else {
                                        histogram_data.push([x1 as f64, y1]);
                                        histogram_data.push([x2 as f64, y2]);
                                        if self.polys {
                                            plot_ui.polygon(
                                                Polygon::new(vec![
                                                    [x1 as f64, 0.0],
                                                    [x1 as f64, y1],
                                                    [x2 as f64, y2],
                                                    [x2 as f64, 0.0],
                                                ])
                                                .stroke(Stroke::NONE)
                                                .color(*color)
                                                .name(format!("Stored: {}", title)),
                                            );
                                        }
                                    }
                                }

                                plot_ui.line(
                                    Line::new(histogram_data)
                                        .color(*color)
                                        .name(format!("Stored: {}", title)),
                                );
                            }
                        });
                });
            });
        });
    }
}

fn main() {
    let icon_path = "icon.png";
    let mut options: NativeOptions = NativeOptions::default();
    if let Ok(icon_image) = image::open(icon_path) {
        let (width, height) = icon_image.dimensions();
        let icon_data = IconData {
            rgba: icon_image.as_bytes().to_vec(),
            width,
            height,
        };
        options.icon_data = Some(icon_data);
    }

    println!("Starting!");
    eframe::run_native(
        "DDO Damage Calculator",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    )
    .expect("Run Native Failed");
}
